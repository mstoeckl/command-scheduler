#ifndef TESTS_H
#define TESTS_H

#include <iostream>

bool test_simple(std::ostream &output);
bool test_triggers(std::ostream &output);
bool test_groups(std::ostream &output);
bool test_complex_groups(std::ostream &output);
bool test_group_reqs(std::ostream &output);
bool test_linear_group(std::ostream &output);
bool test_parallel_group(std::ostream &output);
bool test_mixed_group(std::ostream &output);

#endif //SIMPLE_H_
