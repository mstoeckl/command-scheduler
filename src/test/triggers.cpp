#include "tests.h"
#include "util.h"

using namespace std;

class TestSystem: public RSystem {
public:
	TestSystem() :
			RSystem() {
	}
	virtual ~TestSystem() {
	}
	virtual void iter() {
	}
};

class EnergyCommand: public RCommand {
public:
	int level_;
	EnergyCommand(TestSystem* s, int level) :
			RCommand() {
		level_ = level;
		require(s);
	}
	virtual ~EnergyCommand() {
	}

	virtual void init() {
//		cout << "Started Command: Level " << level_ << endl;
	}
	virtual void iter() {
	}
	virtual bool isDone() {
		return false;
	}
	virtual void close(bool abort) {
//		cout << "Closing Command: Level " << level_ << " by "
//				<< (abort ? "force" : "free will") << endl;
	}
};

bool test_triggers(ostream& o) {
	RScheduler h;

	TestSystem s;

	EnergyCommand e0(&s, 0);
	EnergyCommand e1(&s, 1);
	EnergyCommand e2(&s, 2);

	s.setDefaultCommand(&e0);

	ExternalTrigger r1;
	r1.add(&e1);

	ExternalTrigger r2;
	r2.add(&e2);

	h.addSystem(&s);
	h.addTrigger(&r1);
	h.addTrigger(&r2);

	for (int i = 0; i < 5; i++) {
		h.run(RScheduler::kEnabled);
	}
	r1.flip();
	for (int i = 0; i < 5; i++) {
		h.run(RScheduler::kEnabled);
	}
	r2.flip();
	for (int i = 0; i < 5; i++) {
		h.run(RScheduler::kEnabled);
	}

	return (s.getCurrentCommand() == &e2);
}
