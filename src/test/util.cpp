#include "util.h"

using namespace std;

list<RCommand*> tl(RCommand *x) {
	list<RCommand*> r;
	r.push_back(x);
	return r;
}

list<RCommand*> tl(RCommand *x, RCommand *y) {
	list<RCommand*> r;
	r.push_back(x);
	r.push_back(y);
	return r;
}

WaitKTicks::WaitKTicks(int k, std::ostream* o) :
		RCommand() {
	target_ = k;
	ticks_ = 0;
	out_ = o;
}
WaitKTicks::~WaitKTicks() {
}
void WaitKTicks::init() {
	ticks_ = 0;
}
void WaitKTicks::iter() {
	ticks_++;
	(*out_) << "TICK " << target_ << ": " << ticks_ << endl;
}
bool WaitKTicks::isDone() {
	return ticks_ >= target_;
}
void WaitKTicks::close(bool b) {
}

TimedCommand::TimedCommand(RCommand& c, int timeout, RScheduler &s) :
		RCommand() {
	target_ = timeout;
	ticks_ = 0;
	sub_ = &c;
	sched_ = &s;
}
TimedCommand::~TimedCommand() {
}
void TimedCommand::init() {
	ticks_ = 0;
	sched_->tryCommand(sub_);
}
void TimedCommand::iter() {
	ticks_++;
}
bool TimedCommand::isDone() {
	return ticks_ >= target_ || !sub_->isRunning();
}
void TimedCommand::close(bool b) {
	sched_->removeCommand(sub_);
}

ExternalTrigger::ExternalTrigger() :
		RTrigger() {
	high_ = false;
}
void ExternalTrigger::flip() {
	high_ = !high_;
}
bool ExternalTrigger::get() {
	return high_;
}

SimpSystem::SimpSystem() :
		RSystem() {
}
SimpSystem::~SimpSystem() {
}
void SimpSystem::iter() {
}

SimpCommand::SimpCommand() :
		RCommand() {
}
SimpCommand::~SimpCommand() {
}

void SimpCommand::init() {
}
void SimpCommand::iter() {
}
bool SimpCommand::isDone() {
	return false;
}
void SimpCommand::close(bool b) {
}

void printRun(ostream* o, vector<RCommand*>* r) {
	for (vector<RCommand*>::iterator i = r->begin(); i != r->end(); i++) {
		(*o) << (*i)->isRunning() << " ";
	}
	(*o) << endl;
}
