#include "tests.h"
#include "util.h"
using namespace std;

typedef struct {
	SimpSystem *s;
	SimpCommand *c;
} SimplePair;

SimplePair getPair() {
	SimplePair s;
	SimpSystem* s1 = new SimpSystem();
	SimpCommand* c1 = new SimpCommand();
	c1->require(s1);
	s1->setDefaultCommand(c1);
	s.s = s1;
	s.c = c1;
	return s;
}

double time_simple(int wide, int deep) {
	RScheduler r;
	clock_t start = clock();

	for (int i = 0; i < wide; i++) {
		r.addSystem(getPair().s);
	}
	for (int i = 0; i < deep; i++) {
		r.run(RScheduler::kEnabled);
	}
	for (int i = 0; i < deep; i++) {
		r.run(RScheduler::kDisabled);
	}
	set<RSystem*> sys = r.getActiveSystems();
	for (set<RSystem*>::iterator i = sys.begin(); i != sys.end(); i++) {
		delete (*i)->getDefaultCommand();
		delete (*i);
	}

	clock_t stop = clock();
	return ((double) (stop - start)) / (double) CLOCKS_PER_SEC;
}

void do_timing() {
	const int dstep = 100;
	const int hstep = 100;
	const int dcount = 3;
	const int hcount = 3;
	for (int h = 1; h <= hcount; h++) {
		for (int d = 1; d <= dcount; d++) {
			double time = time_simple(h*hstep, d * dstep);
			cout << time << " ";
		}
		cout << endl;
	}
}

bool test_simple(ostream& o) {
	RScheduler h;
	SimpSystem s;
	SimpCommand c;
	c.require(&s);
	s.setDefaultCommand(&c);
	h.addSystem(&s);

	h.run();
	h.run();

	return c.isRunning();
}
