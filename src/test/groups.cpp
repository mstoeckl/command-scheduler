#include "tests.h"
#include "util.h"
#include <sstream>
using namespace std;

bool test_linear_group(ostream& o) {
	ostream nix(0);

	RScheduler h;

	WaitKTicks w5(5, &o);
	WaitKTicks w4(4, &o);
	WaitKTicks w3(3, &o);
	WaitKTicks w2(2, &o);
	WaitKTicks w1(1, &o);

	DualGroup s;
	s.addSequential(w5);
	s.addSequential(w4);
	s.addSequential(w3);
	s.addSequential(w2);
	s.addSequential(w1);

	vector<RCommand*> w;
	w.push_back(&w5);
	w.push_back(&w4);
	w.push_back(&w3);
	w.push_back(&w2);
	w.push_back(&w1);
	w.push_back(&s);

	h.tryCommand(&s);

	for (int i = 0; i < 15; i++) {
		o << endl;
		h.run();
		printRun(&o, &w);
	}

	return !s.isRunning();
}

bool test_parallel_group(std::ostream &out) {
	ostream nix(0);

	RScheduler h;

	WaitKTicks wA(10, &nix);
	WaitKTicks wB(8, &nix);
	WaitKTicks wC(7, &nix);
	WaitKTicks wD(6, &nix);
	WaitKTicks wE(5, &nix);

	DualGroup p;
	p.addParallel(wA);
	p.addParallel(wB);
	p.addParallel(wC);
	p.addParallel(wD);
	p.addParallel(wE);

	vector<RCommand*> w;
	w.push_back(&wA);
	w.push_back(&wB);
	w.push_back(&wC);
	w.push_back(&wD);
	w.push_back(&wE);
	w.push_back(&p);

	h.tryCommand(&p);

	for (int i = 0; i < 10; i++) {
		h.run();
		printRun(&out, &w);
	}

	return !p.isRunning();
}
bool test_mixed_group(std::ostream &out) {
	return false;
}

bool test_complex_groups(ostream& out) {
	RScheduler h;

	CGroup c;
	WaitKTicks k(20, &out);
	WaitKTicks j(22, &out);
	WaitKTicks l(24, &out);
	WaitKTicks m(30, &out);
	WaitKTicks o(1000, &out);

	c.addSeed(&k);
	c.addSeed(&m);
	c.addNext(&j, tl(&k));
	c.addNext(&l, tl(&k, &l), tl(&m));

	c.addSeed(&o, tl(&l));

	c.rebuildGraph();

	h.tryCommand(&c);
	for (int i = 0; i < 100; i++) {
		out << endl << "Round: " << i << endl;
		h.run();
	}

	return l.isRunning() && !o.isRunning();
}

bool test_groups(ostream& o) {
	RScheduler h;

	const int x = 50;
	const int y = 30;
	const int z = 20;
	const int term = x + y + z + 2;

	CGroup c;
	WaitKTicks five(x, &o);
	WaitKTicks three(y, &o);
	WaitKTicks four(z, &o);

	c.addSeed(&three);
	c.addNext(&four, tl(&three));
	c.addNext(&five, tl(&four));

	c.rebuildGraph();

	h.tryCommand(&c);
	for (int i = 0; i < term; i++) {
		h.run();
	}

	bool pre = c.isRunning();
	h.run();
	bool post = c.isRunning();

	return pre && !post;
}

bool test_group_reqs(ostream &out) {
	ostream nix(0);

	SimpSystem s1;

	SimpCommand low1;
	low1.require(&s1);
	s1.setDefaultCommand(&low1);

	SimpSystem s2;

	SimpCommand low2;
	low2.require(&s2);
	s2.setDefaultCommand(&low2);

	SimpCommand high1;
	high1.require(&s1);

	SimpCommand high2;
	high2.require(&s2);

	SimpCommand highU;
	highU.require(&s1);
	highU.require(&s2);

	WaitKTicks wait5(5, &nix);

	CGroup c;
	c.addSeed(&high1);
	c.addSeed(&high2);
	c.addSeed(&wait5);
	c.addNext(&highU, tl(&wait5));

	ExternalTrigger t;
	t.add(&c);

	RScheduler h;
	h.addSystem(&s1);
	h.addSystem(&s2);
	h.addTrigger(&t);

	vector<RCommand*> r;
	r.push_back(&low1);
	r.push_back(&low2);
	r.push_back(&high1);
	r.push_back(&high2);
	r.push_back(&highU);
	r.push_back(&c);

	for (int i = 0; i < 10; i++) {
		printRun(&out, &r);
		h.run();
	}
	t.flip();
	for (int i = 0; i < 6; i++) {
		printRun(&out, &r);

		h.run();
	}
	ostringstream o1;
	printRun(&o1, &r);
	string pre = o1.str();

	h.run();

	ostringstream o2;
	printRun(&o2, &r);
	string post = o2.str();

	printRun(&out, &r);

	return pre.find("0 0 1 1 0 1") == 0 && post.find("0 0 0 0 1 1") == 0;
}

