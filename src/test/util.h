#ifndef TEST_UTIL_H
#define TEST_UTIL_H

#include "../base/base.h"
#include <iostream>

class WaitKTicks: public RCommand {
private:
	int ticks_;
	int target_;
	std::ostream* out_;
public:
	WaitKTicks(int k, std::ostream* o);
	virtual ~WaitKTicks() ;
	virtual void init();
	virtual void iter();
	virtual bool isDone();
	virtual void close(bool b);
};

class TimedCommand: public RCommand {
public:
	TimedCommand(RCommand& c, int timeout, RScheduler &s);
	virtual ~TimedCommand();
	virtual void init();
	virtual void iter();
	virtual bool isDone();
	virtual void close(bool b);
private:
	int ticks_;
	int target_;
	RCommand* sub_;
	RScheduler* sched_;
};

class ExternalTrigger: public RTrigger {
private:
	bool high_;
public:
	ExternalTrigger();
	void flip();
	bool get();
};

class SimpSystem: public RSystem {
public:
	SimpSystem();
	virtual ~SimpSystem();
	virtual void iter();
};

class SimpCommand: public RCommand {
public:
	SimpCommand();
	virtual ~SimpCommand();

	virtual void init();
	virtual void iter();
	virtual bool isDone();
	virtual void close(bool b);
};

std::list<RCommand*> tl(RCommand *x);

std::list<RCommand*> tl(RCommand *x, RCommand *y);


void printRun(std::ostream* o, std::vector<RCommand*>* r);

#endif // TEST_UTIL_H
