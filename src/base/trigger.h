#ifndef RTRIGGER_H_
#define RTRIGGER_H_

#include "sched.h"
#include "command.h"

#include <set>
 
class RTrigger {
public:
    typedef enum {kRising, kFalling, kWhileHigh, kWhileLow} Condition;
    typedef enum {kStart, kEnd, kToggle} Output;

    RTrigger();
    virtual ~RTrigger();

    void poll(RScheduler &sched);
    void add(RCommand* com, Condition t=kRising, Output o=kStart);
    void stop();

    virtual bool get()=0;
private:
    bool last_;
    bool running_;

    class CommandChange;

    void launchGroup(RScheduler &sched, std::set<CommandChange> &s);

    std::set<CommandChange> while_high_;
    std::set<CommandChange> while_low_;
    std::set<CommandChange> rising_;
    std::set<CommandChange> falling_;
};

#endif // RTRIGGER_H_
