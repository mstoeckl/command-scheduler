#ifndef BASE_METHODS_H
#define BASE_METHODS_H

#include <set>

template<typename T>
void moveToLeft(std::set<T> &a, std::set<T> &b) {
	typedef typename std::set<T>::iterator It;
	It bi = b.begin();
	It be = b.end();
	for (; bi != be; bi++) {
		a.insert(*bi);
	}
	b.clear();
}

template<typename T>
bool doOverlap(std::set<T> &a, std::set<T> &b) {
	typedef typename std::set<T>::iterator It;
	for (It bi = b.begin(); bi != b.end(); bi++) {
		if (a.count(*bi) > 0) {
			return true;
		}
	}
	return false;
}

#endif // BASE_METHODS_H
