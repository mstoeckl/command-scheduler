#include "system.h"

RSystem::RSystem() {
	normal_command_ = 0;
	current_command_ = 0;
}
RSystem::~RSystem() {
}

void RSystem::setDefaultCommand(RCommand* c) {
	normal_command_ = c;
}
RCommand* RSystem::getDefaultCommand() {
	return normal_command_;
}

void RSystem::setCurrentCommand(RCommand* c) {
	current_command_ = c;
}
RCommand* RSystem::getCurrentCommand() {
	return current_command_;
}
