#ifndef CGROUP_H_
#define CGROUP_H_

#include "command.h"

#include <list>
#include <set>
#include <map>
#include <vector>

class CGEntry;

// 
// A generic, flexible command group
// "Seed" commands are those that begin running
// at the initialization of the command group. They
// have no predecessors
// Commands begin when the last command
// in its list of predecessors has stopped, and
// stop when the first of their terminators stops.
//
// Most non-looping directed command graphs can be modeled with this
// and with filler commands
//

// Note: do NOT change requirements of contained Commands
// while running.
// We deliberately do NOT allow removing nodes, or adding them while the command is running
class CGroup : public RCommand {
public:
    CGroup();
    virtual ~CGroup();

    void addSeed(RCommand* s,
    		std::list<RCommand*> terminators=std::list<RCommand*>());

    void addNext(RCommand* s,
    		std::list<RCommand*> predecessors,
    		std::list<RCommand*> terminators=std::list<RCommand*>());
    int getSize();

    // done only during init; converts
    // entries into graph of dependencies
    // (startables and stoppables) of commands
    // Effectively makes the dual of seeds_ , body_,
    // maps those to active_ , corresp. available_.
    void rebuildGraph();

protected:
    virtual void init();
    virtual void iter();
    virtual void close(bool aborted);
    virtual bool isDone();
private:

    // if there is a new addition
    bool newrule_;
    
    void mergeReqs(RCommand* r);

    // lists all the entries, easier to iterate over..
    std::vector<CGEntry*> entries_;
    // lookup from commands to their entries
    std::map<RCommand*, CGEntry> elook_;

    void expandDeath(CGEntry* n, std::set<CGEntry*> *unresolvedB, std::set<CGEntry*> *unresolvedK);
    void terminateConflicting(CGEntry* r);
    void lifeAndDeath();
};

#endif // CGROUP_H_
