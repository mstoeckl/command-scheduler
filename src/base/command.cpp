#include "command.h"
#include "system.h"

using namespace std;

// how to ensure requirements are defined
// at creation: put them ONLY in the
// constructor, as a set<RSystem*>
RCommand::RCommand() :
	running_(false),
	works_in_disabled_(false),
	requirements_()
{
}

RCommand::~RCommand() {

}

void RCommand::require(RSystem* s) {
	requirements_.insert(s);
}
void RCommand::clearRequirement(RSystem* s) {
	requirements_.erase(s);
}
set<RSystem*> RCommand::getRequirements() {
	return requirements_;
}
bool RCommand::isRequirement(RSystem* s) {
	return requirements_.count(s);
}

void RCommand::setWorksInDisabled(bool yes) {
	works_in_disabled_ = yes;
}
bool RCommand::getWorksInDisabled() {
	return works_in_disabled_;
}

bool RCommand::isRunning() {
	return running_;
}

bool RCommand::run() {
	if (!running_) {
		init();
		running_ = true;
	}

	iter();

	if (isDone()) {
		close(true);
		running_ = false;
	}
	return running_;
}

void RCommand::halt() {
	if (!running_) {
		return;
	}
	close(true);
	running_ = false;
}

