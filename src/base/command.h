#ifndef RCOMMAND_H_
#define RCOMMAND_H_

#include <set>
#include <string>

class RSystem;

class RCommand {
public:
    RCommand();
    virtual ~RCommand();

    void require(RSystem* s);
    void clearRequirement(RSystem* s);
    std::set<RSystem*> getRequirements();
    bool isRequirement(RSystem*);

    void setWorksInDisabled(bool yes);
    bool getWorksInDisabled();

    bool isRunning();

    bool run();
    void halt();
protected:
    virtual void init() = 0;
    virtual void iter() = 0;
    virtual void close(bool aborted) = 0;
    virtual bool isDone() = 0;
private:

    bool running_;
    bool works_in_disabled_;

    std::set<RSystem*> requirements_;
};

#endif // RCOMMAND_H_
