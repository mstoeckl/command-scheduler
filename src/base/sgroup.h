#ifndef SGROUP_H_
#define SGROUP_H_

#include "cgroup.h"

// takes responsibility for commands passed in
class SubCGroup: public RCommand {
public:
	SubCGroup();
	virtual ~SubCGroup();
protected:
	class SSCGroup: public CGroup {
	public:
		SSCGroup();
		virtual ~SSCGroup();
		virtual void init();
		virtual void iter();
		virtual void close(bool aborted);
		virtual bool isDone();
	};
	SSCGroup sub_;
private:
	virtual void init();
	virtual void iter();
	virtual void close(bool aborted);
	virtual bool isDone();
};

// a reasonably simple wrapper on top of commands
class DualGroup: public SubCGroup {
public:
	DualGroup();
	virtual ~DualGroup();

	void addSequential(RCommand& c);
	void addParallel(RCommand& c);
private:
	RCommand* prev_;
};

#endif // SGROUP_H_
