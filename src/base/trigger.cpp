#include "trigger.h"

using namespace std;

class RTrigger::CommandChange {
public:
	CommandChange(RCommand* a, Output b) {
		command = a;
		type = b;
	}
	RCommand* command;
	Output type;
	bool operator<(const CommandChange &rhs) const {
		return command < rhs.command;
	}
};

RTrigger::RTrigger() {
	running_ = false;
	last_ = false;
}
RTrigger::~RTrigger() {
}

void RTrigger::poll(RScheduler &sched) {
	if (running_) {
		bool next = get();

		if (next) {
			if (last_) {
				launchGroup(sched, while_high_);
			} else {
				launchGroup(sched, rising_);
			}
		} else {
			if (!last_) {
				launchGroup(sched, while_low_);
			} else {
				launchGroup(sched, falling_);
			}
		}
		last_ = next;
	} else {
		running_ = true;
		last_ = get();
	}
}
void RTrigger::add(RCommand* com, RTrigger::Condition t, RTrigger::Output o) {
	CommandChange c(com, o);
	switch (t) {
	case kRising:
		rising_.insert(c);
		break;
	case kFalling:
		falling_.insert(c);
		break;
	case kWhileHigh:
		while_high_.insert(c);
		break;
	case kWhileLow:
		while_low_.insert(c);
		break;
	}
}
void RTrigger::stop() {
	running_ = false;
}
void RTrigger::launchGroup(RScheduler &sched, set<RTrigger::CommandChange> &s) {
	typedef set<CommandChange>::iterator CCIt;
	for (CCIt i = s.begin(); i != s.end(); i++) {
		CommandChange c = (*i);
		RCommand* r = c.command;
		switch (c.type) {
		case kStart:
			if (!r->isRunning()) {
				sched.tryCommand(r);
			}
			break;
		case kEnd:
			if (r->isRunning()) {
				sched.removeCommand(r);
			}
			break;
		case kToggle:
			if (r->isRunning()) {
				sched.removeCommand(r);
			} else {
				sched.tryCommand(r);
			}
			break;
		}
	}
}

