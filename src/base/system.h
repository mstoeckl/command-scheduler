#ifndef RSYSTEM_H_
#define RSYSTEM_H_

class RCommand;

class RSystem {
public:
    RSystem();
    virtual ~RSystem();

    void setDefaultCommand(RCommand* c);
    RCommand* getDefaultCommand();

    void setCurrentCommand(RCommand* c);
    RCommand* getCurrentCommand();

    virtual void iter()=0;
private:
    RCommand* normal_command_;
    RCommand* current_command_;
};

#endif // RSYSTEM_H_
