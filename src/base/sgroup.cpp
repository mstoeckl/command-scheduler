#include "sgroup.h"

using namespace std;

SubCGroup::SSCGroup::SSCGroup() :
		CGroup() {
}
SubCGroup::SSCGroup::~SSCGroup() {
}
void SubCGroup::SSCGroup::init() {
	CGroup::init();
}
void SubCGroup::SSCGroup::iter() {
	CGroup::iter();
}
void SubCGroup::SSCGroup::close(bool aborted) {
	CGroup::close(true);
}
bool SubCGroup::SSCGroup::isDone() {
	return CGroup::isDone();
}

SubCGroup::SubCGroup() :
		RCommand(), sub_() {
}
SubCGroup::~SubCGroup() {
}

void SubCGroup::init() {
	sub_.init();
}
void SubCGroup::iter() {
	sub_.iter();
}
void SubCGroup::close(bool aborted) {
	sub_.close(aborted);
}
bool SubCGroup::isDone() {
	return sub_.isDone();
}

// DualGroup

DualGroup::DualGroup() : SubCGroup() {
	prev_ = 0;
}
DualGroup::~DualGroup() {
}

void DualGroup::addParallel(RCommand& c) {
	RCommand* s = &c;
	if (prev_ == 0) {
		sub_.addSeed(s);
	} else {
		list<RCommand*> q;
		q.push_back(prev_);
		sub_.addNext(s, q);
	}
}

void DualGroup::addSequential(RCommand& c) {
	addParallel(c);
	// rebind the starting point
	prev_ = &c;
}

