#ifndef RSCHEDULER_H_
#define RSCHEDULER_H_

#include <deque>
#include <list>
#include <set>

// todo: pass references instead of pointers, then take ref address

class RCommand;
class RSystem;
class RTrigger;

class RScheduler {
public:
    typedef enum {kDisabled, kEnabled} Mode;

    RScheduler();
    virtual ~RScheduler();

    //
    // Adds a command if the relevant subsystems are present
    void tryCommand(RCommand* c);
    // queued
    void addSystem(RSystem* s);
    // queued
    void addTrigger(RTrigger* t);
    
    // but these operations are instant
    void removeCommand(RCommand* c);
    void removeSystem(RSystem* s);
    void removeTrigger(RTrigger* t);
    void wipeCommands();
    void wipeTriggers();
    void wipeSystems();

    // accessors; gives the current state, not including
    // queued commands
    std::set<RCommand*> getActiveCommands();
    std::set<RSystem*> getActiveSystems();
    std::set<RTrigger*> getActiveTriggers();
    
    // Stage 1: Add new stuff (system, command, trigger)
    // Stage 2: Update
    //     a) Update triggers
    //     b) Update commands
    //     c) Update systems
    // Stage 3: Add default commands
    void run(Mode m=kEnabled);
private:
    std::set<RSystem*> new_systems_;
    std::set<RCommand*> new_commands_;
    std::set<RTrigger*> new_triggers_;

    std::set<RSystem*> systems_;
    std::set<RCommand*> commands_;
    std::set<RTrigger*> triggers_;
};

#endif
