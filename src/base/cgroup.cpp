#include "cgroup.h"
#include "methods.h"
#include <iostream>

using namespace std;

typedef list<RCommand*> CoList;
typedef CoList::iterator CoIter;
typedef vector<CGEntry*> EnVec;
typedef EnVec::iterator EnIter;

typedef set<CGEntry*> EnSet;

typedef enum {
	kDead, kAlive, kDying, kGrowing
} Mode;

class CGEntry {
public:
	CGEntry() {
		isSeed = false;
		command = 0;
		preds = CoList();
		terms = CoList();
		state = kDead;
	}
	CGEntry(bool seed, RCommand* r, CoList pre, CoList ter) {
		isSeed = seed;
		command = r;
		preds = pre;
		terms = ter;
		state = kDead;
	}
	// the command it links to
	RCommand* command;
	// its command's predecessors
	CoList preds;
	// its command's terminators
	CoList terms;
	// is the command a seed
	bool isSeed;

	// state of the entry now
	Mode state;

	// entries that could be born once the command dies
	EnVec b_deps;
	// entries that could be killed once the command dies
	EnVec k_deps;

	bool operator<(const CGEntry &rhs) const {
		return command < rhs.command;
	}

	bool terminable() {
		for (CoIter i = terms.begin(); i != terms.end(); i++) {
			RCommand* r = (*i);
			if (!r->isRunning()) {
				return true;
				break;
			}
		}
		return true;
	}

	bool generable() {
		for (CoIter i = preds.begin(); i != preds.end(); i++) {
			RCommand* r = (*i);
			if (r->isRunning()) {
				return false;
				break;
			}
		}
		return true;
	}
};

CGroup::CGroup() :
		RCommand() {
	newrule_ = false;
}
CGroup::~CGroup() {

}

void CGroup::addSeed(RCommand* s, CoList terminators) {
	if (isRunning()) {
		cout << "Nope, Command Group is running. Will not add" << endl;
		return;
	}
	newrule_ = true;
	if (elook_.count(s) > 0) {
		cout << "WARNING: Command already added. Undefined behavior" << endl;
	}
	mergeReqs(s);
	elook_[s] = CGEntry(true, s, CoList(), terminators);
	entries_.push_back(&elook_[s]);
}
void CGroup::addNext(RCommand* s, CoList predecessors, CoList terminators) {
	if (isRunning()) {
		cout << "Nope, Command Group is running. Will not add" << endl;
		return;
	}
	newrule_ = true;
	if (elook_.count(s) > 0) {
		cout << "WARNING: Command already added. Undefined behavior" << endl;
	}
	mergeReqs(s);
	elook_[s] = CGEntry(false, s, predecessors, terminators);
	entries_.push_back(&elook_[s]);
}
int CGroup::getSize() {
	return entries_.size();
}
void CGroup::mergeReqs(RCommand* r) {
	typedef set<RSystem*> SySe;
	SySe s = r->getRequirements();
	for (SySe::iterator i = s.begin(); i != s.end(); i++) {
		require(*i);
	}
}

void CGroup::rebuildGraph() {
	if (isRunning()) {
		cout << "Nope, Command Group is running. Will not rebuild" << endl;
		return;
	}

	for (EnIter i = entries_.begin(); i != entries_.end(); i++) {
		CGEntry* e = *i;
		for (CoIter j = e->preds.begin(); j != e->preds.end(); j++) {
			CGEntry* f = &elook_[*j];
			f->b_deps.push_back(e);
			//cout << "BL: " << f->command << " ==> " << e->command << endl;
		}
		for (CoIter j = e->terms.begin(); j != e->terms.end(); j++) {
			CGEntry* f = &elook_[*j];
			f->k_deps.push_back(e);
			//cout << "KL: " << f->command << " ==> " << e->command << endl;
		}
	}

	newrule_ = false;
}
void CGroup::init() {
	if (newrule_) {
		rebuildGraph();
		newrule_ = false;
	}
	for (EnIter i = entries_.begin(); i != entries_.end(); i++) {
		CGEntry* e = *i;
		e->state = e->isSeed ? kGrowing : kDead;
	}
}

// TODO: what happens if a group is in born and kill start simultaneously? duh, ignore kill. Bad luck
void CGroup::iter() {
	lifeAndDeath();

	for (EnIter i = entries_.begin(); i != entries_.end(); i++) {
		CGEntry* e = *i;
		if (e->state == kAlive) {
			bool live = e->command->run();
			if (!live) {
				e->state = kDying;
			}
		}
	}

	lifeAndDeath();
}

void CGroup::lifeAndDeath() {
	// shift growing (to alive) and resolve requirements
	// shift the dying (to dead) and track to spawn or kill their dependencies,
	// process the living (->run())
	// shift all states (nxt --> cur).
	// so, 5 cycles
	for (EnIter i = entries_.begin(); i != entries_.end(); i++) {
		CGEntry* e = *i;
		if (e->state == kGrowing) {
			e->state = kAlive;
			terminateConflicting(e);
		}
	}

	// why not a naive death-propagation? because that takes time:
	// we want instant resolution. Since death is not cyclical, this works
	EnSet unresolvedB;
	EnSet unresolvedK;
	for (EnIter i = entries_.begin(); i != entries_.end(); i++) {
		CGEntry* e = *i;
		//cout << e->command << " | " << e->state << endl;
		if (e->state == kDying) {
			//	cout << e->command << endl;
			e->command->halt();
			e->state = kDead;

			expandDeath(e, &unresolvedB, &unresolvedK);
		}
	}

	// propagate the mark of kDead (they are mutually exclusive), then kBorn
	// kDead acts immediately, kBorn not so

	// this terminates, because it is restricted to living commands.
	// the size of unresolvedK increases only when a command dies, and
	// decreases otherwise
	while (unresolvedK.size() > 0) {
		CGEntry* n = *unresolvedK.begin();
		if (n->terminable()) {
			n->command->halt();
			n->state = kDead;

			expandDeath(n, &unresolvedB, &unresolvedK);
		}
		unresolvedK.erase(n);
	}

	for (EnSet::iterator i = unresolvedB.begin(); i != unresolvedB.end(); i++) {
		CGEntry* b = *i;
		if (b->generable()) {
			b->state = kGrowing;
		}
	}
	unresolvedB.clear();
}

void CGroup::expandDeath(CGEntry* n, EnSet *unresolvedB, EnSet *unresolvedK) {
	EnVec togrow = n->b_deps;
	for (EnIter i2 = togrow.begin(); i2 != togrow.end(); i2++) {
		CGEntry* s = (*i2);
		if (s->state == kDead || s->state == kDying) {
			unresolvedB->insert(s);
		}
	}
	EnVec tokill = n->k_deps;
	for (EnIter i2 = tokill.begin(); i2 != tokill.end(); i2++) {
		CGEntry* s = (*i2);
		if (s->state == kAlive) {
			// all the growth was resolved above; we do not interfere with it
			unresolvedK->insert(s);
		}
	}
}

void CGroup::terminateConflicting(CGEntry* r) {
	RCommand* c = r->command;
	for (EnIter i = entries_.begin(); i != entries_.end(); i++) {
		CGEntry* e = *i;
		if (e == r)
			continue;
		if (e->state == kAlive || e->state == kGrowing) {

			RCommand* k = e->command;
			set<RSystem*> r1 = c->getRequirements();
			set<RSystem*> r2 = k->getRequirements();
			if (doOverlap(r1, r2)) {
				if (e->state == kGrowing) {
					cout
							<< "Two growing elements conflict requirements. Killing one."
							<< endl;
				}
				e->state = kDying;
			}

		}
	}
}

void CGroup::close(bool aborted) {
	if (aborted) {
		for (EnIter i = entries_.begin(); i != entries_.end(); i++) {
			CGEntry* e = *i;
			RCommand* r = e->command;
			if (r->isRunning())
				r->halt();
		}
	}
}
bool CGroup::isDone() {
	for (EnIter i = entries_.begin(); i != entries_.end(); i++) {
		CGEntry* e = *i;
		if (e->state == kAlive || e->state == kGrowing || e->state == kDying) {
			return false;
		}
	}
	return true;
}

