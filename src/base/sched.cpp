#include "sched.h"
#include "trigger.h"
#include "system.h"
#include "command.h"
#include "methods.h"

#include <iostream>

using namespace std;

typedef set<RSystem*>::iterator SysIt;
typedef set<RCommand*>::iterator ComIt;
typedef set<RTrigger*>::iterator TrigIt;

RScheduler::RScheduler() {
}
RScheduler::~RScheduler() {

}

void RScheduler::tryCommand(RCommand* c) {
	if (commands_.count(c) > 0) {
		return;
	}
	new_commands_.insert(c);
}
void RScheduler::addSystem(RSystem* s) {
	if (systems_.count(s) > 0)
		return;
	new_systems_.insert(s);
}
void RScheduler::addTrigger(RTrigger* t) {
	if (triggers_.count(t) > 0)
		return;
	new_triggers_.insert(t);
}

void RScheduler::removeCommand(RCommand* c) {
	new_commands_.erase(c);
	commands_.erase(c);

	if (c->isRunning()) {
		c->halt();
	}

	set<RSystem*> deps = c->getRequirements();
	for (SysIt si = deps.begin(); si != deps.end(); si++) {
		RSystem* l = (*si);
		l->setCurrentCommand(0);
	}
}
void RScheduler::removeSystem(RSystem* s) {
	new_systems_.erase(s);
	systems_.erase(s);

	set<RCommand*> cp = commands_;
	for (ComIt i=cp.begin();i!=cp.end();i++) {
		RCommand* c = (*i);
		if (c->isRequirement(s)) {
			removeCommand(c);
		}
	}
}
void RScheduler::removeTrigger(RTrigger* t) {
	new_triggers_.erase(t);
	triggers_.erase(t);
	t->stop();
}
void RScheduler::wipeCommands() {
	while (new_commands_.size() > 0)
		removeCommand(*(new_commands_.begin()));
	while (commands_.size() > 0)
		removeCommand(*(commands_.begin()));
}
void RScheduler::wipeTriggers() {
	while (new_triggers_.size() > 0)
		removeTrigger(*(new_triggers_.begin()));
	while (triggers_.size() > 0)
		removeTrigger(*(triggers_.begin()));
}
void RScheduler::wipeSystems() {
	while (new_systems_.size() > 0)
		removeSystem(*(new_systems_.begin()));
	while (systems_.size() > 0)
		removeSystem(*(systems_.begin()));
}

set<RCommand*> RScheduler::getActiveCommands() {
	return commands_;
}
set<RSystem*> RScheduler::getActiveSystems() {
	return systems_;
}
set<RTrigger*> RScheduler::getActiveTriggers() {
	return triggers_;
}

void RScheduler::run(RScheduler::Mode m) {
	if (new_systems_.size() > 0) {
		moveToLeft(systems_, new_systems_);
	}
	if (new_commands_.size() > 0) {
		for (ComIt nir = new_commands_.begin(); nir != new_commands_.end();
				nir++) {
			RCommand* c = (*nir);

			bool depexists = true;
			set<RSystem*> deps = c->getRequirements();
			for (SysIt sir = deps.begin(); sir != deps.end(); sir++) {
				if (systems_.count(*sir) == 0) {
					depexists = false;
					break;
				}
			}

			if (!depexists) {
				continue;
			}

			// full steam ahead!
			for (SysIt sir = deps.begin(); sir != deps.end(); sir++) {
				RSystem* s = (*sir);
				RCommand* u = s->getCurrentCommand();
				if (u != 0) {
					removeCommand(u);
				}
				s->setCurrentCommand(c);
			}

			commands_.insert(c);
		}
		new_commands_.clear();
	}
	if (new_triggers_.size() > 0) {
		moveToLeft(triggers_, new_triggers_);
	}

	set<RTrigger*> tp = triggers_;
	for (TrigIt tr = tp.begin(); tr != tp.end(); tr++) {
		(*tr)->poll(*this);
	}

	set<RCommand*> cp = commands_;
	for (ComIt cr = cp.begin(); cr != cp.end(); cr++) {
		RCommand* c = (*cr);
		if (m == RScheduler::kEnabled || c->getWorksInDisabled()) {
			bool live = c->run();
			if (!live) {
				removeCommand(c);
			}
		} else {
			removeCommand(c);
		}
	}

	set<RSystem*> sp = systems_;
	for (SysIt sr= sp.begin(); sr != sp.end(); sr++) {
		(*sr)->iter();
	}

	for (SysIt sr= sp.begin(); sr != sp.end(); sr++) {
		RSystem* s = (*sr);
		RCommand* cur = s->getCurrentCommand();
		if (cur == 0) {
			RCommand* def = s->getDefaultCommand();
			if (def != 0) {
				tryCommand(def);
			}
		}
	}

}
