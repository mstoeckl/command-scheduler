#include <iostream>
#include "test/tests.h"

using namespace std;

void runtest(bool on, bool (*f)(ostream &x), string name) {
	bool success;
	if (on) {
		success = (*f)(cout);
	} else {
		ostream nix(0);
		success = (*f)(nix);
	}
	cout << "Test:" << name << " " << (success ? "Passed" : "Failed") << endl;
}
const bool F = false;
const bool T = true;

int main() {
	runtest(F, test_simple, "Simple");
	runtest(F, test_triggers, "Triggers");
	runtest(F, test_groups, "Groups");
	runtest(F, test_complex_groups, "Complex");
	runtest(F, test_group_reqs, "Group Reqs");
	runtest(F, test_linear_group, "Linear Group");
	runtest(F, test_parallel_group, "Parallel Group");
	runtest(F, test_mixed_group, "Mixed Group");
	return 0;
}
