##### Goal:

To realize a scheduling system like WPILibs but without the looming background Scheduler and the omnipresent static subsystems

##### TODO:

1. Tweak the Command Group to be safe against infinite loops and prevent the 1-cycle lag between termination and generation of commands therein.
2. License
3. Develop a mock structure for a robot based on this scheduler
